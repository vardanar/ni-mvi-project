import keras.layers as layers
from keras.models import Model

img_width = 320
img_height = 256


def unet_conv(x, y, z, to):
    conv = layers.Conv2D(x, (y, z), padding="same", activation="relu")(to)
    return layers.BatchNormalization()(conv)


def unet_downconv(x, y, z, to):
    c = unet_conv(x, y, z, to)
    c = unet_conv(x, y, z, c)
    return c, layers.MaxPooling2D(pool_size=(2, 2), padding="same", strides=(2, 2))(c)


def unet_upconv(x, y, z, to1, to2):
    upsam = layers.UpSampling2D((2, 2))(to1)
    m1 = layers.Concatenate(axis=-1)([upsam, to2])
    c = unet_conv(x, y, z, m1)
    return unet_conv(x, y, z, c)


def unet_model(activation="sigmoid"):
    inputs = layers.Input(shape=(img_height, img_width, 6))

    conv1, pool1 = unet_downconv(32, 3, 3, inputs)
    conv2, pool2 = unet_downconv(64, 3, 3, pool1)
    conv3, pool3 = unet_downconv(128, 3, 3, pool2)
    conv4, pool4 = unet_downconv(256, 3, 3, pool3)

    conv5 = unet_conv(512, 3, 3, pool4)
    conv5 = unet_conv(512, 3, 3, conv5)

    conv6 = unet_upconv(256, 3, 3, conv5, conv4)
    conv7 = unet_upconv(128, 3, 3, conv6, conv3)
    conv8 = unet_upconv(64, 3, 3, conv7, conv2)
    conv9 = unet_upconv(32, 3, 3, conv8, conv1)

    conv10 = layers.Conv2D(3, (1, 1), activation=activation, padding="same")(conv9)

    return Model(inputs, conv10)
